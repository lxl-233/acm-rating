// const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

module.exports = {
  devServer: {
    // proxy: 'http://localhost:8181/'
    proxy: {
      '/api': {
        target: 'https://api.acm.xiaolongli.top/',
        // target: 'http://localhost:8081/',
        changeOrigin: true
      },
      '/graphql': {
        target: 'https://leetcode-cn.com/',
        changeOrigin: true
      },
      '/nowcoder': {
        target: 'https://ac.nowcoder.com/',
        changeOrigin: true,
        pathRewrite: {
          '^/nowcoder': ''
        }
      },
      '/vjudge': {
        target: 'https://vjudge.net/',
        changeOrigin: true,
        pathRewrite: {
          '^/vjudge': ''
        }
      },
      '/luogu': {
        target: 'https://www.luogu.com.cn/api/',
        changeOrigin: true,
        pathRewrite: {
          '^/luogu': ''
        }
      },
      '/hdu': {
        target: 'http://acm.hdu.edu.cn/',
        changeOrigin: true,
        pathRewrite: {
          '^/hdu': ''
        }
      },
      '/codeforces': {
        target: 'https://codeforces.com/',
        changeOrigin: true,
        pathRewrite: {
          '^/codeforces': ''
        }
      },
      '/loj': {
        target: 'https://api.loj.ac.cn/api/',
        changeOrigin: true,
        pathRewrite: {
          '^/loj': ''
        },
        headers: {
          referer: 'https://api.loj.ac.cn/api/cors/xdomain.html',
          origin: 'https://api.loj.ac.cn'
        }
      }
    }
    // proxy: 'http://localhost:8181/'
  },
  chainWebpack: (config) => {
    config.plugin('html').tap((args) => {
      args[0].title = 'HENUACM-RATING'
      return args
    })
  },
  productionSourceMap: false,
  configureWebpack: {
    name: 'ACM-RATING',
    devServer: {
      https: true,
      host: '127.0.0.1'
    },
    plugins: [
      //   new BundleAnalyzerPlugin()
    ],
    resolve: {
      extensions: ['.js', '.json', '.vue', '.scss', '.css'],
      alias: {
        '@': require('path').resolve(__dirname, 'src')
      }
    },
    externals: {
      vue: 'Vue',
      vuex: 'Vuex',
      'vue-router': 'VueRouter',
      'element-ui': 'ELEMENT',
      Axios: 'axios'
    }
  }
}
