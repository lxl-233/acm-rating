# ACM-rating 前端

- 项目在线地址 [acm-rating](http://acm.xiaolongli.top/)
- 后端项目请访问 [acm-后端](https://gitee.com/lxl-233/acm-rating-background)  

### 功能

- ~~博客功能~~
- OJ 爬取用户记录，现在支持 `CF、牛客、HDU、vjudge、LOJ`，`luogu`只能拿到全部的题数

### 技术栈

- 前端 [Vue框架](https://cn.vuejs.org/v2/guide/)
- 组件 [Element UI](https://element.eleme.cn/#/zh-CN/component/installation)

---
### 项目下载

>  推荐使用`git`克隆

```shell
git clone https://gitee.com/lxl-233/acm-rating
```

---
### 项目环境

- [Node.js](https://nodejs.org/zh-cn/) 环境
- 使用Vue 脚手架的 [图形化界面](https://nodejs.org/zh-cn/) 创建项目，依赖和插件尽量用图形化界面安装，例如：`axios插件`，`element插件`，等具体依赖查看   [pagckage.json](https://gitee.com/lxl-233/acm-rating/blob/master/package.json)

---

### 项目启动

1. 安装用到的依赖


```
npm install
```

2.  启动服务

```
npm run serve
```

3. 打包项目（这是项目放到云服务器用的

```
npm run build
```

### 项目的源码的目录结构

|     名称     |                             作用                             |
| :----------: | :----------------------------------------------------------: |
|   `assets`   |                       静态文件（图片等                       |
|   `common`   |                          放公用的js                          |
| `components` |                      大部分是公用的组件                      |
|  `networks`  |                    封装异步请求地址的函数                    |
|  `plugins`   |               下载的插件`axios.js、element.js`               |
|   `router`   |                           路由管理                           |
|   `store`    | [Vue.js 应用程序开发的**状态管理模式**](https://vuex.vuejs.org/zh/) |
|   `views`    |                             页面                             |



##### 修改自己后端的请求地址

`vue.config.js` 修改，和上边修改类似，把代理的地址修改后端的请求地址

```js
module.exports = {
   proxy: {
      '/api': {
        target: 'https://api.acm.xiaolongli.top/', # 你的后端地址
        // target: 'http://localhost:8081/',
        changeOrigin: true
      },
   }
}

```

#### 线上nginx部署配置

nginx配置文件： [nginx.conf](./nginx/acm.conf)，修改`proxy_pass` 你的后端地址

```nginx
location ^~ /api/ {
		add_header 'Access-Control-Allow-Origin' '*';
		add_header 'Access-Control-Allow-Credentials' 'true';
		add_header 'Access-Control-Allow-Methods' '*';
		proxy_pass https://api.acm.xiaolongli.top; # 后端地址
	}
```

---

> 项目想维护或者想增加新的功能，请联系作者 `QQ:1392728166`

