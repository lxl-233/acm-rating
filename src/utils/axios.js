'use strict'
import store from '@/store'
import axios from 'Axios'
import router from '@/router'
// Full config:  https://github.com/axios/axios#request-config
// axios.defaults.baseURL = process.env.baseURL || process.env.apiUrl || '';
// axios.defaults.headers.common['Authorization'] = AUTH_TOKEN;
// axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';

const config = {
  // baseURL: 'http://39.96.59.80:8181/',
  // baseURL: 'http://47.93.9.196:8181/api'
  // baseURL: 'http://localhost:8181/',
  // baseURL: process.env.baseURL || process.env.apiUrl || ""
  // timeout: 60 * 1000, // Timeout
  // withCredentials: true, // Check cross-site Access-Control

}

const _axios = axios.create(config)

_axios.interceptors.request.use(
  function(config) {
    // Do something before request is sent
    if (localStorage.getItem('token')) {
      config.headers.token = localStorage.getItem('token')
    }
    return config
  },
  function(error) {
    // Do something with request error
    return Promise.reject(error)
  }
)

// Add a response interceptor
_axios.interceptors.response.use(
  function(response) {
    // Do something with response data
    return response
  },
  function(error) {
    if (error.response) {
      if (error.response.status === 401) {
        store.commit('del')
        router.push({
          path: '/login',
          query: { redirect: router.currentRoute.fullPath }// 登录成功后跳入浏览的当前页面
        }).then(r => r).catch(r => r)
      }
    }
    // Do something with response error
    return Promise.reject(error)
  }
)

export default _axios
