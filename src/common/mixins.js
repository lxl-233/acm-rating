
export const verifyMixin = {
  data() {
    return {
      identifyCodes: '1234567890',
      identifyCode: ''
    }
  },
  created() {
    this.refreshCode()
  },
  methods: {
    randomNum(min, max) {
      return Math.floor(Math.random() * (max - min) + min)
    },
    refreshCode() {
      this.identifyCode = ''
      this.makeCode(this.identifyCodes, 4)
    },
    makeCode(o, l) {
      for (let i = 0; i < l; i++) {
        this.identifyCode += this.identifyCodes[
          this.randomNum(0, this.identifyCodes.length)
        ]
      }
    }
  }
}

export const resultColorMixin = {
  data() {
    return {
      results: [
        'wrong answer',
        'runtime error',
        'accepted',
        'compilation error',
        'time limit',
        'memory limit',
        'happy new year',
        'hacked'
      ],
      colors: [
        '#DB2828',
        '#FBBD08',
        '#21BA45',
        '#767676',
        '#F2711C',
        '#F2711C',
        '#21BA45',
        '#DB2828'
      ]
    }
  },
  methods: {
    getResultColor(val) {
      const style = { color: '#767676', fontWeight: '600' }
      if (val === null) {
        return style
      }
      val = val.toLowerCase()
      const index = this.results.findIndex(res => val.indexOf(res) !== -1)
      if (index !== -1) style.color = this.colors[index]
      return style
    }
  }
}

export const resultCfColorMixin = {
  data() {
    return {
      ratings: [
        1199, 1399, 1599, 1899,
        2099, 2399, 2999, 9999999
      ],
      colors: [
        '#808080', '#008000', '#03A89E', '#0000FF',
        '#AA00AA', '#FF8C00', '#FF0000', '#380101'
      ]
    }
  },
  methods: {
    getResultColor(val) {
      const index = this.ratings.findIndex(res => res >= val)
      return { 'color': this.colors[index], fontWeight: 600 }
    }
  }
}
