const Vuex = require('vuex')

export default new Vuex.Store({
  state: {
    token: localStorage.getItem('token') ? localStorage.getItem('token') : '',
    user: localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')) : {}
  },
  mutations: {
    set(state, data) {
      state.token = data.token
      state.user = data.user
      localStorage.setItem('token', data.token)
      localStorage.setItem('user', JSON.stringify(data.user))
    },
    del(state) {
      state.token = ''
      state.user = {}
      localStorage.removeItem('token')
      localStorage.removeItem('user')
    },
    updateAvatar({ user }, data) {
      user.userImage = data
      localStorage.setItem('user', JSON.stringify(user))
    },
    setUser({ user }, data) {
      user = data
      localStorage.setItem('user', JSON.stringify(user))
    }
  },
  actions: {},
  modules: {},
  getters: {
    avatar(state) {
      return JSON.stringify(state.user) === {} ? '#' : state.user.userImage
    },
    isLogin(state) {
      return JSON.stringify(state.user) === '{}'
    }
  }
})
