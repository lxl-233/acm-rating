import axios from '@/utils/axios'
export function createBlog(data) {
  return axios.post('/api/createBlog', data)
}

export function updateLike(data) {
  return axios.post('/api/updateLike', data)
}

export function detailBlog(id) {
  return axios.get('/api/detailBlog', { params: { id: id }})
}

export function myBlog() {
  return axios.post('/api/myBlog')
}

export function getBlog() {
  return axios.post('/api/getAllBlog')
}

export function newBlog(data) {
  return axios.post('/api/newBlog', data)
}
