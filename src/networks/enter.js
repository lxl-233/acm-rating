import axios from '@/utils/axios'
export function register(form) {
  return axios.post('/api/register', form)
}

export function login(form) {
  return axios.post('/api/login', form)
}

export function getUser(data) {
  return axios.post('/api/getUser', data)
}

export function sendVerifyCode(email) {
  return axios.get('/api/sendVerifyCode', { params: email })
}

export function modifyPassword(data) {
  return axios.post('/api/modifyPassword', data)
}
