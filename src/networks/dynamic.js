import axios from '@/utils/axios'
export function dynamic(data) {
  return axios.post('/api/dynamic', data)
}

export function getCfScore() {
  return axios.get('/api/getCfScore')
}

export function mySolved(id) {
  return axios.get('/api/mySolved', { params: { id: id }})
}

export function weekStar() {
  return axios.get('/api/weekStar')
}

export function crawlerOj(data) {
  return axios.post('/api/crawlerOj', data)
}

