import axios from '@/utils/axios'
export function uploadImage(data) {
  return axios.post('/api/uploadImage', data, {
    headers: { 'content-type': 'multipart/form-data' }
  })
}

export function imgAdd(data) {
  return axios.post('/api/imgAdd', data, {
    headers: { 'content-type': 'multipart/form-data' }
  })
}

export function updateInfo(data) {
  return axios.post('/api/updateInfo', data)
}

export function updateOjAccount(data) {
  return axios.post('/api/bindOjAccount', data)
}

export function getOjAccount() {
  return axios.get('/api/getOjAccount')
}

export function profile(id) {
  return axios.get('/api/profile', { params: { id: id }})
}

export function crawler(oj) {
  return axios.get('/api/crawler', { params: { oj }})
}
