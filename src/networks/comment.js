import axios from '@/utils/axios'
export function sendMessage(data) {
  return axios.post('/api/sendMessage', data)
}

export function sendChildMessage(data) {
  return axios.post('/api/sendChildMessage', data)
}

export function getComments(data) {
  return axios.post('/api/getComments', data)
}
