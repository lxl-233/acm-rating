import axios from '@/utils/axios'

export function showRank(data) {
  return axios.post('/api/showRank', data)
}
