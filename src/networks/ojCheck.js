import axios from '@/utils/axios'
// extend with Request#proxy()
export function apiLeetcode(username) {
  const data = {
    query:
      'query userPublicProfile($userSlug:String!){userProfilePublicProfile(userSlug:$userSlug){submissionProgress{totalSubmissions acTotal}}}',
    variables: { userSlug: username }
  }
  return axios.post('/graphql/', data)
}

export function apiNowcoder(username) {
  return axios.get(`/nowcoder/acm/contest/profile/${username}/`)
}

export function apiVjudge(username) {
  const url = `/vjudge/user/solveDetail/${username}`
  return axios.get(url)
}

export function apiLoj(username) {
  const url = `/loj/user/getUserDetail`
  const data = {
    now: new Date(),
    timezone: 'Asia/Shanghai',
    username
  }
  return axios.post(url, data)
}

export function apiLuogu(username) {
  const url = `/api/luogu/user/search?keyword=${username}`
  return axios.get(url)
}

export function apiHdu(username) {
  const url = `/hdu/userstatus.php?user=${username}`
  return axios.get(url)
}

export function apiCodeforces(username) {
  const url = `/codeforces/search?by=&query=${username}`
  return axios.get(url)
}
