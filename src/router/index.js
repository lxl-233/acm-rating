
const VueRouter = require('vue-router')
const Index = () => import('@/views/Index/Index')
const Login = () => import('@/views/Enter/Login')
const Forget = () => import('@/views/Enter/Forget')
const Register = () => import('@/views/Enter/Register')
const RealtimeStatus = () => import('@/views/RealtimeStatus/RealtimeStatus')
const Rank = () => import('@/views/Rank/Rank')
const Setting = () => import('@/views/Setting/Setting')
const Blog = () => import('@/views/Blog/Blog')
const NewBlog = () => import('@/views/Blog/NewBlog')
const UpdateBlog = () => import('@/views/Blog/UpdateBlog')
const DetailBlog = () => import('@/views/Blog/DetailBlog')
const Profile = () => import('@/views/Setting/Profile')
const Error = () => import('@/components/Error')

const routes = [
  {
    path: '/',
    name: 'index',
    component: Index,
    meta: {
      title: 'HENUACM-RATING',
      content: {
        keywords:
          'HENUACM-RATING,ACM,ACM-RATING,HENU,HENU-ACM,HENUACM,河南大学软件学院,HENUACM团队管理',
        description:
          '从各个OJ （在线判题系统）为每个用户爬取刷题记录，对每周，每月，每年做一个排名，选出上一周最多刷题数量的用户，对团队 CF rating 排名 ，发布团队比赛信息，发布个人总结，每场比赛的总结，现在支持 CF、牛客、HDU、vjudge、LOJ、Leetcode、Luogu'
      }
    }
  },
  { path: '/login', name: 'login', component: Login },
  { path: '/register', name: 'register', component: Register },
  {
    path: '/realtimeStatus',
    name: 'realtimeStatus',
    component: RealtimeStatus
  },
  {
    path: '/rank',
    name: 'rank',
    component: Rank,
    meta: {
      title: 'HENUACM-RANK',
      content: {
        keywords: 'HENUACM-RANK,ACM团队刷题排名',
        description:
          '从各个OJ （在线判题系统）为每个用户爬取刷题记录，对每周，每月，每年做一个排名'
      }
    }
  },
  { path: '/setting', name: 'setting', component: Setting },
  { path: '/blog', name: 'blog', component: Blog },
  { path: '/newBlog', name: 'newBlog', component: NewBlog },
  { path: '/forget', name: 'forget', component: Forget },
  { path: '/detailBlog', name: 'detailBlog', component: DetailBlog },
  { path: '/profile', name: 'profile', component: Profile },
  { path: '/updateBlog', name: 'updateBlog', component: UpdateBlog },
  { path: '*', name: 'Error', component: Error }
]

const router = new VueRouter({
  routes,
  mode: 'history',
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
      return { x: 0, y: 0 }
    }
  }
})

router.beforeEach((to, from, next) => {
  /* 路由发生变化修改页面meta */
  console.log(to.meta.content)
  if (to.meta.content) {
    const head = document.getElementsByTagName('head')
    const meta = document.createElement('meta')
    document
      .querySelector('meta[name="keywords"]')
      .setAttribute('content', to.meta.content.keywords)
    document
      .querySelector('meta[name="description"]')
      .setAttribute('content', to.meta.content.description)
    meta.content = to.meta.content
    head[0].appendChild(meta)
  }
  // /* 路由发生变化修改页面title */
  if (to.meta.title) {
    document.title = to.meta.title
  }
  next()
})

export default router
